import React, { Component } from 'react';
import { Container, Content, Text } from 'native-base';
import CheckInCard from './CheckInCard';
import PrivateCheckInCard from './PrivateCheckInCard';

export default class MainScene extends Component {
    constructor(props) {
        super(props);
        this.state = { data : [], un: '' };
    }

    componentDidMount() {
        if(this.props.showall) {
            fetch('https://stark-sands-31617.herokuapp.com/api/m4Card', {
                method: 'GET',
            })
                .then(response => response.json())
                .then(responseJson => this.setState({ data: responseJson}));
        } else {
            fetch('https://stark-sands-31617.herokuapp.com/api/m4CardbyName', {
                method: 'GET',
            })
                .then(response => response.json())
                .then(responseJson => this.setState({ data: responseJson}));
        }

        fetch('https://stark-sands-31617.herokuapp.com/username', {
                method: 'GET'
            })
                .then(response => response.json())
                .then(responseJson => console.log(responseJson.username));
    }

    render() {
        const cards = this.state.data.map((d) =>
            {
                if (this.props.showall) {
                    return <CheckInCard key={d.Id} data={d} nav={this.props.nav} />
                } else {
                    return <PrivateCheckInCard key={d.Id} data={d} nav={this.props.nav} />
                }
                
            }
        );

        return (
            <Container>
                <Content>
                    <Text>{this.state.un}</Text>
                    {cards}
                </Content>
            </Container>
        );
    }
}