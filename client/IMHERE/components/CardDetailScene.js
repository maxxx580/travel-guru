import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { Container, Content, Card, CardItem, Icon, Text, List, ListItem, Button } from 'native-base';

export default class CardDetailScene extends Component {
    constructor(props) {
        super(props);
        this.state = { data : {} };
    }
    
    componentDidMount() {
        var self = this;

        fetch('https://stark-sands-31617.herokuapp.com/api/m4CardbyID/' + this.props.cardid, { method: 'GET' })
                .then((response) => response.json())
                .then((responseJson) => self.setState({ data: responseJson}));
    }

    render() {
        const la = this.state.data.Lat;
        const ln = this.state.data.Lng;
        const laln = la + ',' + ln;
        console.log('https://maps.googleapis.com/maps/api/staticmap?size=450x200&zoom=10&markers=color:red%7C' + laln + '&key=AIzaSyDONROpk_1o3_lUWpplT84dgaMW5bWIT7M');
        return (
            <Container>
                <Content>
                    <Card>
                        <CardItem>
                            <Image
                                source={{uri: 'https://maps.googleapis.com/maps/api/staticmap?size=450x200&zoom=10&markers=color:red%7C' + laln + '&key=AIzaSyDONROpk_1o3_lUWpplT84dgaMW5bWIT7M'}}
                                resizeMode="contain"
                                style={{ height: 200 }}
                            />
                        </CardItem>
                        <CardItem>
                            <Icon name='md-pin' />
                            <Text>{this.state.data.Location}</Text>
                            <Text note>by {this.state.data.Username} on {this.state.data.Time}</Text>
                        </CardItem>
                        <CardItem>
                            <Image source={{uri: this.state.data.ImgSource}} />
                        </CardItem>
                        <CardItem>
                            <Text>{this.state.data.Desc}</Text>
                        </CardItem>
                        <CardItem>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                <Button transparent>
                                    <Icon name='md-thumbs-up' /><Text>{this.state.data.Likes}</Text>
                                </Button>
                                <Button transparent>
                                    <Icon name='md-thumbs-down' /><Text>{this.state.data.Dislikes}</Text>
                                </Button>
                            </View>
                        </CardItem>
                        <CardItem>
                            <List dataArray={this.state.data.Comments}
                                renderRow={(item) =>
                                    <ListItem>
                                        <Text>{item.Commentor}: {item.Comment}</Text>
                                    </ListItem>    
                                }
                            >
                            </List>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}