import React, { Component } from 'react';
import { View, Text, Platform, PixelRatio, StyleSheet, CameraRoll, Image, TouchableOpacity } from 'react-native';
import ImagePicker from 'react-native-image-picker';

export default class CameraScene extends Component {
    state = {
        avatarSource: null
    };

    selectPicture() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };

        var self = this;

        ImagePicker.showImagePicker(options, (response) => {
            this.setState({
                avatarSource: {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true}
            });

            self.props.nav.push({sceneIndex: 2, base64pic: this.state.avatarSource.uri})
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this.selectPicture.bind(this)}>
                    <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
                        { this.state.avatarSource === null ? <Text>Select a Photo</Text> :
                            <Image style={styles.avatar} source={this.state.avatarSource} />
                        }
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    avatarContainer: {
        borderColor: '#9B9B9B',
        borderWidth: 1 / PixelRatio.get(),
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatar: {
        borderRadius: 0,
        width: 250,
        height: 250
    }
});