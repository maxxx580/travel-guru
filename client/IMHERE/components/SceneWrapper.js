import React, { Component, PropTypes } from 'react';
import { View, Button, Text, TouchableHighlight } from 'react-native';
import LoginScene from './LoginScene';
import MainScene from './MainScene';
import AddNewScene from './AddNewScene';
import CardDetailScene from './CardDetailScene';
import CameraScene from './CameraScene';

export default class SceneWrapper extends Component {
    render() {
        if (this.props.sceneIdx === 0) {    
            return <LoginScene nav={this.props.nav} />
        } else if (this.props.sceneIdx === 1) {
            return <MainScene showall={true} nav={this.props.nav} />
        } else if (this.props.sceneIdx === 2) {
            return <AddNewScene nav={this.props.nav} base64pic={this.props.base64pic} />
        } else if (this.props.sceneIdx === 3) {
            return <MainScene showall={false} nav={this.props.nav} />
        } else if (this.props.sceneIdx === 4) {
            return <CardDetailScene cardid={this.props.cardid} />
        } else if (this.props.sceneIdx === 5) {
            return <CameraScene nav={this.props.nav} />
        } 
    }
}

// sceneIndex:
//   0: login scene
//   1: main scene, plaza
//   2: add new scene
//   3: my cards scene
//   4: card detail scene
//   5: camera scene