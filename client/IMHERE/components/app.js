import React, { Component } from 'react';
import { BackAndroid, DrawerLayoutAndroid, Navigator, Button } from 'react-native';
import SceneWrapper from './SceneWrapper';
import DrawerContents from './DrawerContents';


function initBackButton(myNavigator) {
    BackAndroid.addEventListener('hardwareBackPress', function() {
        myNavigator.pop();
        return true;
    });
}

export default class IMHERE extends Component {
    componentDidMount() {
        initBackButton(this.refs.navigator);
    }

    render() {
        return (
            <DrawerLayoutAndroid
                drawerWidth={200}
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={( () => <DrawerContents nav={this.refs.navigator} drawr={this.refs.drawer}/>)}
                ref='drawer'>

                <Navigator
                    ref="navigator"
                    initialRoute={{ sceneIndex: 0 }}
                    renderScene={(route, navigator) =>
                        <SceneWrapper sceneIdx={route.sceneIndex} nav={navigator} cardid={route.referenceID} base64pic={route.base64pic} />
                    }
                    configureScene={(route, routeStack) =>
                        Navigator.SceneConfigs.FadeAndroid}
                />
            </DrawerLayoutAndroid>
        )
    }
}