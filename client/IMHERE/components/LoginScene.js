import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Button
} from 'react-native';

export default class LoginScene extends Component {
    constructor(props) {
        super(props);
        this.state= { text: 'umn1' };
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Please name yourself:
                </Text>
                <TextInput
                    style={styles.inputBar}
                    onChangeText={(text) => this.setState({text})}
                    value={this.state.text}
                />
                <Button
                    title="OK!"
                    onPress={ () => {
                        fetch('https://stark-sands-31617.herokuapp.com/setusername/' + this.state.text, {
                            method: 'GET'
                        });
                        this.props.nav.push({sceneIndex: 1});
                    }}
                    style={{width: 50}}
                />
            </View>
        );
    }
}

// TODO: set session name

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        margin: 50
    },
    welcome: {
        fontSize: 28,
        textAlign: 'center',
        margin: 10,
    },
    inpurBar: {
        height: 26,
        borderWidth: 0.5,
        borderColor: '#0f0f0f',
        flex: 1,
        fontSize: 20,
        padding: 4,
        textAlign: 'center',        
    }
});