import React, { Component } from 'react';
import { Container, Content, Card, CardItem, Button, Text, Icon } from 'native-base';
import { View, TextInput, Image } from 'react-native';

export default class AddNewScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            location: '',
            lat: 0.0,
            lng: 0.0,
            desc: 'Tell everyone what you find!'
        };
        this.saveNew = this.saveNew.bind(this);
    }

    saveNew() {
        var self = this;

        var d = new Date();

        var jsonBall = {
            Location: this.state.location,
            Lat: this.state.lat,
            Lng: this.state.lng,
            Base64Img: this.props.base64pic,
            DateTime: d.toDateString(),
            Desc: this.state.desc
        }

        fetch('https://stark-sands-31617.herokuapp.com/api/m4Card', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(jsonBall)
        }).then(() => self.props.nav.push({ sceneIndex: 1 }));
    }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                var geocodingurl = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + ',' + position.coords.longitude + '&key=AIzaSyDONROpk_1o3_lUWpplT84dgaMW5bWIT7M';

                fetch(geocodingurl, { method: 'GET' })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        this.setState({ location : responseJson.results[2].formatted_address });
                    })
                    .catch((error) => {
                        alert(error);
                    });

                this.setState({ lat: position.coords.latitude, lng: position.coords.longitude })
            },
            (error) => alert(JSON.stringify(error)),
            {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
        );
    }

    render() {
        return (
            <Container>
                <Content>
                    <Card>
                        <CardItem>
                            <Image resizeMode="contain" source={{uri: this.props.base64pic}} />
                        </CardItem>
                        <CardItem>
                            <Icon name='md-pin' />
                            <Text>{this.state.location}</Text>
                        </CardItem>
                        <CardItem>
                            <TextInput
                                onChangeText={(text) => this.setState({ desc: text })}
                                value={this.state.desc}
                                numberOfLines={4}
                                editable = {true}
                                multiline = {true}
                            />
                        </CardItem>
                        <CardItem>
                            <Button block onPress={this.saveNew}> Send! </Button>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}