import React, { Component } from 'react';
import { Card, CardItem, Icon, Text, Button } from 'native-base';
import { View, Image } from 'react-native';

export default class CheckInCard extends Component {
    constructor(props) {
        super(props);
        this.viewDetail = this.viewDetail.bind(this);
        this.onClickLike = this.onClickLike.bind(this);
        this.onClickDisLike = this.onClickDisLike.bind(this);
        this.state = { likenum: this.props.data.Likes, dislikenum: this.props.data.Dislikes };
    }

    viewDetail() {
        this.props.nav.push({sceneIndex: 4, referenceID: this.props.data.Id});
    }

    onClickLike() {
        fetch('https://stark-sands-31617.herokuapp.com/api/m4-like/' + this.props.data.Id);
        this.setState({ likenum: this.state.likenum + 1 });
    }

    onClickDisLike() {
        fetch('https://stark-sands-31617.herokuapp.com/api/m4-dislike/' + this.props.data.Id);
        this.setState({ dislikenum: this.state.dislikenum + 1 });
    }

    render() {
        return (
            <Card>
                <CardItem>
                    <Icon name='md-pin' />
                    <Text>{this.props.data.Location}</Text>
                </CardItem>
                <CardItem button onPress={this.viewDetail} >
                    <Image source={{uri: this.props.data.ImgSource}} style={{ flex: 2}} />
                </CardItem>
                <CardItem>
                    <Text>by {this.props.data.Username}</Text>
                </CardItem>
                <CardItem>
                    <Text>{this.props.data.Desc.substring(0, 120) + '...'}</Text>
                </CardItem>
                <CardItem>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <Button transparent onPress={this.onClickLike}>
                            <Icon name='md-thumbs-up' /><Text>{this.state.likenum}</Text>
                        </Button>
                        <Button transparent onPress={this.onClickDisLike}>
                            <Icon name='md-thumbs-down' /><Text>{this.state.dislikenum}</Text>
                        </Button>
                    </View>
                </CardItem>
            </Card>
        )
    }
}