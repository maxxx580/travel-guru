import React, { Component } from 'react';
import { Card, CardItem, Icon, Text } from 'native-base';
import { Image } from 'react-native';

export default class CheckInCard extends Component {
    constructor(props) {
        super(props);
        this.viewDetail = this.viewDetail.bind(this);
    }

    viewDetail() {
        this.props.nav.push({sceneIndex: 4, referenceID: this.props.data.Id});
    }

    render() {
        return (
            <Card>
                <CardItem>
                    <Icon name='md-pin' />
                    <Text>{this.props.data.Location}</Text>
                </CardItem>
                <CardItem button onPress={this.viewDetail} >
                    <Image source={{uri: this.props.data.ImgSource}} style={{ flex: 2}} />
                </CardItem>
                <CardItem>
                    <Text>by {this.props.data.Username}</Text>
                </CardItem>
                <CardItem>
                    <Text>{this.props.data.Desc.substring(0, 120) + '...'}</Text>
                </CardItem>
            </Card>
        )
    }
}