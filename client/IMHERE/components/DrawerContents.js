import React, { Component } from 'react';
import { Container, Content, List, ListItem, Text, Icon } from 'native-base';

export default class DrawerContents extends Component {
    constructor(props) {
        super(props);
        this.goHome = this.goHome.bind(this);
        this.goToMine = this.goToMine.bind(this);
        this.addNew = this.addNew.bind(this);
    }

    goHome() {
        this.props.nav.push({sceneIndex: 1});
        this.props.drawr.closeDrawer();
    }

    goToMine() {
        this.props.nav.push({sceneIndex: 3});
        this.props.drawr.closeDrawer();
    }

    addNew() {
        this.props.nav.push({sceneIndex: 5});
        this.props.drawr.closeDrawer();
    }

    render() {
        return (
            <Container>
                <Content>
                    <List>
                        <ListItem button onPress={this.addNew}>
                            <Icon name='md-add' />
                            <Text>I'm HERE!</Text>
                        </ListItem>
                        <ListItem button onPress={this.goHome}>
                            <Icon name='md-locate' />
                            <Text>Plaza</Text>
                        </ListItem>
                        <ListItem button onPress={this.goToMine}>
                            <Icon name='md-bookmarks' />
                            <Text>Mine</Text>
                        </ListItem>
                   </List>
                </Content>
            </Container>
        )
    }
}

// sceneIndex:
//   0: login scene
//   1: main scene, plaza
//   2: add new scene
//   3: my cards scene
//   4: card detail scene