var express = require('express');
var router = express.Router();
var request = require('request');

var mongoose = require('mongoose');
mongoose.connect("mongodb://admin:admin@ds129018.mlab.com:29018/module-4");
//define itinerary schema


//============================================================= TODO END

var UserSchema = new mongoose.Schema({
    Username: String
});
var User = mongoose.model('user', UserSchema);

var CardSchema = new mongoose.Schema({
  Id: Number,
  Name: String,
  Username: String,
  // Data: Buffer,
  ContentType: String,
  Description: String,
  Geocode: Object
});
var Card = mongoose.model('card', CardSchema);


//================================================================== Routing
// return all cards
router.get('/card', function(req, res, next) {
  Card.find({Public: true}, function(err, cards) {
    if(err) {
      console.log("error find cards");
    } else {
      res.send(cards);
    }
  });
});

// return cards created by the user
router.get('/mycard', function(req, res, next) {
  Card.find({Username : req.session.username}, function(err, cards) {
    if (cards) {
      res.send(cards);
    } else {
      res.send({msg: 'no card was found'});
    }
  });
});





// 更新by id
// router.put('/itinerary/:id', function (req, res, next) {
//   //
//   Itinerary.findOneAndUpdate(
//     {Id: req.params.id}, 
//     {//Name: req.body.name, 
//      Public: req.body.public,
//      //Url_list: req.body.url_list, 
//      //Tags: req.body.tags
//     },
//     {new: true},
//      function(err, doc) {
//       if(err == null){
//         res.send({message: "Itinerary Not Exits!"});
//       }
//       else{
//         res.send({message: "Itinerary Updated!"});
//       }
//      });
//   // 同样 这里传入的数据有：
//   // req.body.name string
//   // req.body.public true/false
//   // req.body.url_list [...]
//   // req.body.tags [...]
// });

//  add a new card
router.post('/card', function(req, res, next) {
    Card
        .findOne()
        .sort('-Id') //get the max id we have by now so we will not get itinerary with same id
        .exec(function (err, card) {
            if (err) {
                console.log(err);
            } else {
                var newId = card.Id + 1;
                var newCard = new Card({
                    Id: newId,
                    Username: req.body.name,
                    // Data: Buffer,
                    ContentType: req.body.type,
                    Description: req.body.description,
                    Geocode: {lat:0, lang:0}
                });

                console.log(newCard);
                console.log(req.session.username);

                newCard.save(function (err,data) {
                    if(err){
                        console.log(err);
                    } else {
                        res.send({message: "Card is Created!"});
                    }
                });
            }
        }); 
});

//  delete a card
router.delete('/delete/card/:id', function(req, res, next) {
  Card
    .find({Id: req.params.id})
    .remove()
    .exec();
});


//============================================================= 先别动这块儿
// router.post('/locationData', function(req, res, next) {
//   console.log(req.body);
//   request({
//     url: 'https://shutupandgivemethecontent.herokuapp.com/api/article?url=' + req.body.articleurl,
//     json: true,
//   }, function (error, response, body) {
//     if (!error && response.statusCode === 200) {
//       res.send(body);
//     }
//     else {
//       res.status(500).send('API failed responding!');
//     }
//   })
// });













// ====================================================== Junk Code， 没用 参考

/* GET example. */
router.get('/example', function(req, res, next) {
  var foo = {
    message: 'hello from express!'
  }
  res.send(foo);
});


module.exports = router;
