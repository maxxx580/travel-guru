# Module4-group-project #

__Submitted by:__ _Web Buster_

__Team members:__

- jiang872@umn.edu 
- xuxx1018@umn.edu 
- zhan2223@umn.edu 
- maxxx580@umn.edu

Server side: [https://stark-sands-31617.herokuapp.com/](https://stark-sands-31617.herokuapp.com/)

__Argument of ambition:__
This is a place check-in app. With this app, user could record moment when he/she is in a place. It allows user to create a card, upload his/her location information, take a picture and add some description for that place. After the card is created, it could be found in Plaza. And in the Plaza, cards could be liked or disliked by other users.  


__Argument of execution:__
All requirement features in the instruction are completed. The app has multuple of views, Plaze, Mine Cards, and the create new card page. The GPS function on the phone is used to determine the location of the user. The server-side was deployed to the heroku and it handles all the interaction between the client and the database(mongodb). The additional involvement of Google Map in the app shows the location with a small map, makes the location information more readable.



## Intro ##
You've conquered static sites, server-side programming, and client-side
programming. Now the only thing standing between you and total web domination
is _mobile_, but not for long!

Your task now is to create an Android app using _react-native_.

## Requirements ##
You must create a mobile app. What it does exactly is up to you. It must meet
these requirements:

- has multiple views
- utilizes the mobile platform. This means you need to use something
  that you only get on mobile or that mobile makes easy. This could be the
  camera, microphone, gps, etc.
- has a server-side component that enables user-user interaction. This doesn't
  need to be explicit interaction such as messaging, although it can be. It
  just needs to be a way for one of your user's actions to affect another
  user's experience of the app. As an example, giving a user a stream of
  pictures submitted by other users would satisfy this requirement.

## Project proposals ##
Since you are being given a lot of freedom in what your mobile app does we want
to give you a chance to normalize your expectations with your peers. On pitch
day we expect you to have a 2 minute presentation of your idea ready to
present. This should __include sketches__ that illustrate what it will do.
You can give this presentation via slideshow, on paper, or any other means that
you believe can effectively convey your idea.

Pitch day will still run in much the same way as before. Expect to give your
presentation a couple of times to groups of five to ten people.

## A note on reusing code ##
You are free to reuse any code from module 2 or module 3 if you desire to.
However, __this will reduce our evaluation of the technical ambition of your
project__ so you will need to attempt something more technically challenging
overall to achieve the same grade.

## Submission ##
- Your code should be pushed up to your repo on github
- Fill this `README.md` out with your team name and team members' emails

## Grading ##
You will be graded on the __ambition__ and __execution__ of the project. At the
top of this `README.md` you have the opportunity to argue why your submission
was ambitious and well executed. In order for us to grade it, you must have it
hosted on Heroku. To earn an "A" grade, a project must be technically
ambitious, well-executed, and polished. To earn a passing grade, a project must
minimally fulfill the three requirements listed in the description.
